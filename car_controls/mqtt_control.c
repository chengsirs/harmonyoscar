#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include <unistd.h>
#include "hi_wifi_api.h"
#include "hi_time.h"
//#include "wifi_sta.h"
#include "lwip/ip_addr.h"
#include "lwip/netifapi.h"

#include "lwip/sockets.h"

#include "MQTTClient.h"

#include "cJSON.h"

#include "kv_store.h"

#include "car_control.h"

struct opts_struct
{
	char* clientid;
	int nodelimiter;
	char* delimiter;
	enum QoS qos;
	char* username;
	char* password;
	char* host;
	int port;
	int showtopics;
} opts =
{
	(char*)"1319", 1, (char*)"\n", QOS2, (char*)"", (char*)"", (char*)"", 0, 1
};


unsigned char buf[100];
unsigned char readbuf[100];
Network n;
static MQTTClient c;
char *runmsg[1024];	

void messageArrived(MessageData* md)
{
	MQTTMessage* message = md->message;
	cJSON *recvjson;

	if (opts.showtopics)
		printf("topicName %.*s\t", md->topicName->lenstring.len, md->topicName->lenstring.data);
	if (opts.nodelimiter){
		printf("payload %.*s", (int)message->payloadlen, (char*)message->payload);

		recvjson=cJSON_Parse((char*)message->payload);		
		char *type= cJSON_GetObjectItem(recvjson, "actionType")->valuestring;
		char *action= cJSON_GetObjectItem(recvjson, "action")->valuestring;
		car_run(type,action);
		printf("type %s action %s", type,action);
		
    	strcpy(runmsg,"mqtt car run  ");
		strcat(runmsg,action);
		printf("runmsg %s",runmsg);
	}
	else
		printf("payloadlen %.*s%s", (int)message->payloadlen, (char*)message->payload, opts.delimiter);
}


int mqtt_Connect(void)
{
	int rc = 0;
	
	MQTTMessage pubmsg;
	
	char* topic = "car_control"; // car_control

    char *carmqip[32],*carmqport[10], *carmqsub[12], *carmqpush[14], *carmquser[4], *carmqpassword[7] ;

    strcpy(carmqip,"");
    strcpy(carmqport,"");
    strcpy(carmqsub,"");
    strcpy(carmqpush,"");
    strcpy(carmquser,"");
    strcpy(carmqpassword,"");

    UtilsGetValue("carmqsub",carmqsub,sizeof(carmqsub));
	printf("carmqsub is %s\n", carmqsub);
    UtilsGetValue("carmqpush",carmqpush,sizeof(carmqpush));
	printf("carmqpush is %s\n", carmqpush);
    UtilsGetValue("carmqip",carmqip,sizeof(carmqip));
	printf("carmqip is %s\n", carmqip);
    UtilsGetValue("carmqport",carmqport,sizeof(carmqport));
	printf("carmqport is %s\n", carmqport);
    UtilsGetValue("carmquser",carmquser,sizeof(carmquser));
	printf("carmquser is %s\n",carmquser);
    UtilsGetValue("carmqpassword",carmqpassword,sizeof(carmqpassword));
	printf("carmqpassword is %s\n",carmqpassword);

	if (strchr(carmqsub, '#') || strchr(carmqsub, '+')) // topic
		opts.showtopics = 1;
	if (opts.showtopics)
		printf("topic is %s\n", carmqsub); //  topic

	int port=atoi(carmqport);
	printf("Network start %s ,%d  \n", carmqsub,port);

	NetworkInit(&n);
	NetworkConnect(&n,carmqip,port); // opts.host  opts.port
	
	MQTTClientInit(&c, &n, 1000, buf, 100, readbuf, 100);
	MQTTStartTask(&c);

	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;       
	data.willFlag = 0;
	data.MQTTVersion = 3;
	data.clientID.cstring = opts.clientid;
	data.username.cstring = carmquser;// opts.username;
	data.password.cstring = carmqpassword; // opts.password;

	data.keepAliveInterval = 10;
	data.cleansession = 1;
	printf("Connecting to %s %d\n", carmqip, opts.port); // opts.host

	sleep(3);

	while(1){
		rc = MQTTConnect(&c, &data);
		printf("Connected %d\n", rc);
		if(rc==0){
			break;
		}
		MQTTDisconnect(&c);
		sleep(1);
	}
    
	

    printf("Subscribing to %s\n", carmqsub);
	rc = MQTTSubscribe(&c, carmqsub, opts.qos, messageArrived); // topic
	printf("Subscribed %d\n", rc);
	if(rc==0){
		strcpy(runmsg,"小车已订阅MQTT指令！");
	}

	char* pushtopic = carmqpush; // "phone_control"; // phone_control

	memset(&pubmsg, '\0', sizeof(pubmsg));
  	pubmsg.payload = (void*)"                    ";
  	pubmsg.payloadlen = strlen((char*)pubmsg.payload);
  	pubmsg.qos = QOS0;
  	pubmsg.retained = 0;
  	pubmsg.dup = 0;
	
	while (1)
	{
		if(strcmp(runmsg,"")!=0){
			pubmsg.payload = (void*)runmsg;
			printf("pubmsg.payload %s \n",pubmsg.payload);
    		MQTTPublish(&c, pushtopic, &pubmsg);
    		strcpy(runmsg,"");
		}
        // hi_udelay(300*1000);
		sleep(1);	
	}
	
	// printf("Stopping\n");

	// MQTTDisconnect(&c);
	// NetworkDisconnect(&n);

	return 0;
}
