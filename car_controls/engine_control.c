
#include <hi_types_base.h>
#include "iot_gpio.h"
#include "iot_pwm.h"
#include "hi_gpio.h"
#include "hi_io.h"
#include "hi_pwm.h"
#include "hi_time.h"

hi_void set_angle(hi_u32 utime)
{
    IoTGpioInit(HI_GPIO_IDX_2);
    IoTGpioSetFunc(HI_GPIO_IDX_2,HI_IO_FUNC_GPIO_2_GPIO);
    IoTGpioSetDir(HI_GPIO_IDX_2, HI_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(HI_GPIO_IDX_2,HI_GPIO_VALUE1);
    hi_udelay(utime);
    IoTGpioSetOutputVal(HI_GPIO_IDX_2,HI_GPIO_VALUE0);
    hi_udelay(20000-utime);
}


hi_void engine_turn_left(hi_void)
{
    for (int i = 0; i <10; i++) {
        set_angle(2500);
    }
}


hi_void engine_turn_right(hi_void)
{
    for (int i = 0; i <10; i++) {
        set_angle(500);
    }
}


hi_void regress_middle(hi_void)
{
    for (int i = 0; i <10; i++) {
        set_angle(1500);
    }
}