#ifndef ENGINE_CONTROL_H
#define ENGINE_CONTROL_H

#include <hi_types_base.h>

hi_void engine_turn_left(hi_void);

hi_void engine_turn_right(hi_void);

hi_void regress_middle(hi_void);

#endif