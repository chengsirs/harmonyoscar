import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'


const errorHandler = (err, vm, info) => {
uni.showModal({
                   content: "报错原因为: " +err.message+ err.stack,
                   showCancel: false
               });
}
Vue.config.errorHandler = errorHandler;

const app = new Vue({
    ...App
})
app.$mount()
